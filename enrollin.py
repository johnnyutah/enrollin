# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from acctMng import users
import time, re, logging, getpass, sys, datetime
logging.basicConfig(filename='py.log', level=logging.INFO)
#essentially, this script tries to log in to peoplesoft, check the status of the first course in the user's cart, and enroll if it's open
#it is very hacky and ugly and not my best code at all.
#this code is for educational purposes only.

class enrollin():

	def __init__(self):	
		self.driver = webdriver.Firefox()
		self.driver.implicitly_wait(30)
		self.base_url = "https://psmobile.pitt.edu/"
		self.verificationErrors = []
		self.accept_next_alert = True 
		self.waity = 0
		self.conseq = 0
		self.tries = 0 
		self.logit("Test starting!")
		self.loaded = False
		self.Fail = 0
		self.Fail2 = 0

	def enroll(self,user,pw):
		while(1 == 1):
			it = 0
			self.tries+=1
			self.logit("Try " + str(self.tries))
			time.sleep(self.waity)
			self.waity = 0
			if not self.loaded:
				try:
					self.login(user,pw)			
					self.driver.get(self.base_url + "app/student/enrollmentcart/cart")
					self.driver.get(self.base_url + "app/student/enrollmentcart/cart/UPITT/UGRD/2171")
					self.logit("Cart loaded.")
					#driver.save_screenshot("screenie_init_" + str(it) + ".png")
					it+=1
					self.loaded = True
				except Exception as exept:
					print(exept)
					print(sys.exc_info()[0])
					print("oh stuff")
					self.logit("somethin real bad")
					self.Fail +=1
					if(self.Fail >= 3):
						self.logit("aborting")
						return 0
			try:
				#pull up the cart and find the course status
				self.driver.get(self.base_url + "app/student/enrollmentcart/cart")
				status = self.driver.find_element_by_xpath("/html/body/section/section/form/a/div/table/tbody/tr/td[2]/ul/li[5]").text
				self.logit(status)
				self.Fail2 = 0
			except Exception as exept:
				print(exept)
				print(sys.exc_info()[0])
				self.loaded = False
				self.logit("logged the frack out B(")
				self.Fail2 += 1
				if (self.Fail2 >= 5):
					return 0
				time.sleep(60)
			if(self.loaded != False and "Wait List" not in status and "Closed" not in status):
				
				#driver.save_screenshot("screenie_" + str(it) + ".png")
				it+=1
				self.execute()
			else:
				#the course is not open, wait for a decent timeout, and after so many failures, wait a good while
				#randomization could be utilized here to better imitate a human user - undetermined if necessary
				self.logit("can't do stuff")
				
				#driver.save_screenshot("screenie_" + str(it) + ".png")
				it+=1
				self.waity = 30
				if self.conseq == 20:
					self.waity = 60
					self.conseq = 0
				self.conseq+=1
	#login to the form. this is pretty important, we can't do anything if we're not logged in
	def login(self,user,pw):
		loginit = 0	
		try:
			self.driver.get("https://psmobile.pitt.edu/app/profile/login")
			self.logit("Page loaded.")
		except Exception as exept:
			print(exept)
			print(e.format(e.errno, e.strerror))
			self.logit("Can't get no login page.")
			self.logit("error".format(e.errno, e.strerror))
			#self.driver.save_screenshot("screenies/screenie_initload" + str(login_it) + ".png")
			loginit+=1
		
		#self.driver.find_element_by_css_selector("span.block-icon.ico-library").click()
		#self.driver.save_screenshot("screenies/screenie_click_ico.png")
		#try to log in
		try:	
			print("Logging in as ")
			print(user)
			self.driver.find_element_by_name("username").send_keys(user)
			self.driver.find_element_by_name("password").send_keys(pw)
			#self.driver.save_screenshot("screenies/screenie_initload" + str(login_it) + ".png")
			loginit+=1
			self.driver.find_element_by_name("loginAction").click()
		except Exception as exept:
			print(exept)
			print(sys.exc_info()[0])
			self.logit("login failed")	
		self.logit("logged the frack in B)")
		#self.driver.save_screenshot("screenies/screenie_initload" + str(login_it) + ".png")

	#if the first class in the car is open, try to enroll!
	def execute(self):
		try:
			execit = 0
			driver = self.driver 
			driver.find_element_by_id("cart-select-all").click()
			self.logit("all selected.")
			driver.find_element_by_id("enroll").click()
			#driver.save_screenshot("screenies/screenie_exec" + str(execit) + ".png")
			execit+=1
			self.logit("enrolling all...")
			self.logit("they see me scrollin...")
			#driver.save_screenshot("screenies/screenie_exec" + str(execit) + ".png")
			execit+=1
			driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
			driver.find_element_by_xpath("//*[@id=\"confirm-enrollment-dialog\"]/div/div/div[3]/div[1]/label").click()
			self.logit("yes i agree to the terms >_>")
			driver.find_element_by_id("confirm-enrollment-submit").click()
			self.logit("I SAID YES")
			#driver.save_screenshot("screenies/screenie_exec" + str(execit) + ".png")
			execit+=1
			res = driver.find_element_by_xpath("/html/body/section/section/div[3]").text
			driver.find_element_by_link_text("Okay").click()
			self.logit("DONE!!!")
			self.logit("result: " + res)
		except Exception as exept:
			print(exept)
			print(sys.exc_info()[0])
			return 0

	def timeStr(self):
		ret = ""
		now = time.time()
		ret = datetime.datetime.fromtimestamp(now).strftime('%Y-%m-%d %H:%M:%S')
		return ret

	def logit(self, s):
		logging.info( self.timeStr() + " " + s)

if __name__ == "__main__":
	print("user: " + sys.argv[1])
	if len(sys.argv) > 2:
		#if third arg can be supplied (it can be anything)
		#lets use a user supplied pass
		p = getpass.getpass()
	else:
		#if no third argument is supplied, get the password matching the user id
		#from stored passwords -- see acctMng.py
		u = users()
		l = u.getDict()
		p = l[sys.argv[1]]
	while(1 == 1):
		e = enrollin()
		e.enroll(sys.argv[1],p)
