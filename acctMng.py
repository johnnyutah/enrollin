import sys,pickle,getpass
#this python program creates the object users.pickle
#it stores any users added in the users.pickle
#this is in plaintext - it'd be great to implement this securely
class users():
	
	def __init__(self):
		try:
			with open ('users.pickle', 'rb') as handle:
				self.useDict = pickle.load(handle)
		except:
			self.useDict = {"":""}
			self.save()
	
	def add(self,user,pw):
		if self.useDict is None:
			d = {user,pw}
			self.useDict = d
		else:
			self.useDict[user] = pw
		self.save()
	
	def save(self):
		with open('users.pickle','wb') as handle:
			pickle.dump(self.useDict,handle)
	def getDict(self):
		return self.useDict
def main():
	u = users()
	inp = "0"
	while( inp != "3"):
		print("1)add to users\n2)view users\n3)exit")
		inp = raw_input()
		if(inp == "1"):
			user = raw_input("Username?")
			pw = getpass.getpass("Password?")
			u.add(user,pw)	
		elif(inp == "2"): 
			for kk, vv in u.getDict().iteritems():
				print("User: " + kk + " Pass: " + vv)
			
	sys.exit()

if __name__ == "__main__":
	main()
